#include "mainwindow.h"
#include <gtkmm.h>


int main(int argc, char* argv[])
{
    // Initialize gtkmm
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "www.lucidarme.me");
    // Create the window
    mainwindow w;
    // Start main loop
    return app->run(w);
}
