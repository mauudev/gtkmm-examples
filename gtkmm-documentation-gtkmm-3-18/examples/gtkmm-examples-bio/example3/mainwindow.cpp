#include "mainwindow.h"
#include <iostream>


// Constructor of the main Window (build ui interface).
mainwindow::mainwindow()
{
	// Initialize the main window
	this->set_title("Grid");
	this->set_border_width(10);

	// Create and connect the first button
	button1.add_label("Button 1");
	button1.signal_clicked().connect(sigc::mem_fun(*this,&mainwindow::on_button_1));
	mainGrid.attach(button1,0,0,1,1);

	// Create and connect the second button
	button2.add_label("Button 2");
	button2.signal_clicked().connect(sigc::mem_fun(*this,&mainwindow::on_button_2));
	mainGrid.attach(button2,1,0,1,1);

	// Create and connect the Quit button
	buttonQuit.add_label("Quit");
	buttonQuit.signal_clicked().connect(sigc::mem_fun(*this,&mainwindow::close));
	mainGrid.attach(buttonQuit,0,1,2,1);

	// Display the main grid in the main window
	mainGrid.show_all();
	add(mainGrid);
}


// Destructor of the class
mainwindow::~mainwindow()
{}


// Call when the first button is clicked
void mainwindow::on_button_1()
{
	std::cout << "Button 1" << std::endl;
}


// Call when the second button is clicked
void mainwindow::on_button_2()
{
	std::cout << "Button 2" << std::endl;
}
