#include <gtkmm.h>



int main(int argc, char* argv[])
{
    // Initialize gtkmm and create the window
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "www.lucidarme.me");
    Gtk::Window window;

    // Create the vertical layout
    Gtk::VBox mainLayout;
    window.add(mainLayout);


    // Create and add the menuBar to the layout
    Gtk::MenuBar menuBar;
    mainLayout.pack_start(menuBar, Gtk::PACK_SHRINK);


    // _________________________________
    // ::: Create the sub menu Files :::


    // Create the menu item files
    Gtk::MenuItem menuFiles;
    menuFiles.set_label("Files");
    menuBar.append(menuFiles);

    // Create a sub menu in files
    Gtk::Menu subMenuFiles;
    menuFiles.set_submenu(subMenuFiles);

    // Create a menu item from Stock (New)
    Gtk::ImageMenuItem menuNew(Gtk::Stock::NEW);
    subMenuFiles.append(menuNew);

    // Create a menu item with a sub menu (Recent files)
    Gtk::MenuItem menuRecentFiles("Recent files", true);
    subMenuFiles.append(menuRecentFiles);
    Gtk::Menu subMenuRecentFiles;
    menuRecentFiles.set_submenu(subMenuRecentFiles);
    Gtk::MenuItem recentFile1("File1.txt");
    subMenuRecentFiles.append(recentFile1);
    Gtk::MenuItem recentFile2("File2.txt");
    subMenuRecentFiles.append(recentFile2);
    Gtk::MenuItem recentFile3("File3.txt");
    subMenuRecentFiles.append(recentFile3);

    // Create a menu item from Stock (Open)
    Gtk::ImageMenuItem menuOpen(Gtk::Stock::OPEN);
    subMenuFiles.append(menuOpen);

    // Create a menu item from Stock (Save)
    Gtk::ImageMenuItem menuSave(Gtk::Stock::SAVE);
    subMenuFiles.append(menuSave);

    // Add a separator
    Gtk::SeparatorMenuItem hline;
    subMenuFiles.append(hline);

    // Create a menu item from Stock (Close)
    Gtk::ImageMenuItem menuClose(Gtk::Stock::CLOSE);
    subMenuFiles.append(menuClose);

    //  Create and connect a menu item from Stock (Quit)
    Gtk::ImageMenuItem menuQuit(Gtk::Stock::QUIT);
    menuQuit.signal_activate().connect(sigc::ptr_fun(&Gtk::Main::quit));
    subMenuFiles.append(menuQuit);



    // _________________________________
    // ::: Create the sub menu Edit :::


    // Create the menu item Edit
    Gtk::MenuItem menuEdit("Edit", true);
    menuBar.append(menuEdit);

    // Create a sub menu in Edit
    Gtk::Menu subMenuEdit;
    menuEdit.set_submenu(subMenuEdit);

    // Create a menu item from Stock (Cut)
    Gtk::ImageMenuItem menuCut(Gtk::Stock::CUT);
    subMenuEdit.append(menuCut);

    // Create a menu item from Stock (Copy)
    Gtk::ImageMenuItem menuCopy(Gtk::Stock::COPY);
    subMenuEdit.append(menuCopy);

    // Create a menu item from Stock (Paste)
    Gtk::ImageMenuItem menuPaste(Gtk::Stock::PASTE);
    subMenuEdit.append(menuPaste);

    // Create a menu item
    Gtk::MenuItem menuExample("Example");
    subMenuEdit.append(menuExample);



    // Create and add an image in the window
    Gtk::Image image("gtk.png");
    mainLayout.pack_end(image);

    // Show window itens
    window.show_all();


    // Start main loop
	return app->run(window);
    return 0;
}
