#include "mainwindow.h"

// Constructor of the main Window (build ui interface).
mainwindow::mainwindow()
{
    // Resize the main window and set border width
    this->resize(400,200);
    set_border_width(5);

    // Add the label and connect the button
    Button.add_label("Page 1 (button)");
    Button.signal_clicked().connect(sigc::mem_fun(*this,&mainwindow::on_button_clicked));

    // Set label text
    Label.set_text("Page 2 (label)");

    // Add the widgets (button and label) in the notebook
    Selector.append_page(Button, "First");
    Selector.append_page(Label, "Second");

    // This packs the button into the Window (a container).
    add(Selector);
    show_all_children();
}


// Destructor of the class
mainwindow::~mainwindow()
{}

// Call when the button os clicked and display my url
void mainwindow::on_button_clicked()
{
    std::cout << "www.lucidarme.me" << std::endl;
}
