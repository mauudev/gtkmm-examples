#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <gtkmm.h>
#include <iostream>




// The class mainwindow inherits from Gtk::Window
class mainwindow : public Gtk::Window
{
    // Constructor and destructor
public:    
	mainwindow();
	virtual             ~mainwindow();

protected:
    //Signal handlers (run when the button is clicked)
    void                    on_button_clicked();

    //Page selector
    Gtk::Notebook           Selector;

    // Button for the first page
    Gtk::Button             Button;

    // Label for the second page
    Gtk::Label              Label;
};


#endif // MAINWINDOW_H
