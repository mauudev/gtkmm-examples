#include "mainwindow.h"
#include <gtkmm.h>


int main(int argc, char* argv[])
{
    // Initialize gtkmm and create the main window
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "www.lucidarme.me");
    // Create the window
    mainwindow window;
    // Start main loop
    return app->run(window);}
