#include "mainwindow.h"
#include <stdlib.h>
#include <iostream>


// Constructor of the main Window (build ui interface).
mainwindow::mainwindow()
{
	// Initialize the main window
	this->set_title("Scroll bar");
	this->set_border_width(10);

	// Create the first button
	button1.add_label("Display value");
	button1.set_size_request(150,50);
	button1.signal_clicked().connect(sigc::mem_fun(*this,&mainwindow::on_button));
	mainGrid.attach(button1,0,0,1,1);

	// Add the Quit button
	buttonQuit.add_label("Quit");
	buttonQuit.set_size_request(150,50);
	buttonQuit.signal_clicked().connect(sigc::mem_fun(*this,&mainwindow::close));
	mainGrid.attach(buttonQuit,1,0,1,1);

	// Add the scroll bar
	scale.set_range(0,100);
	scale.set_value(50);
	scale.signal_value_changed().connect(sigc::mem_fun(*this,&mainwindow::on_scale_change));
	mainGrid.attach(scale,0,1,2,1);

	// Display the main grid in the main window
	mainGrid.show_all();
	add(mainGrid);
}


// Destructor of the class
mainwindow::~mainwindow()
{}


// Call when the button is clicked
void mainwindow::on_button()
{
	std::cout << "Current value is: " << scale.get_value() << std::endl;
}


// Call when the scroll bar value is changed
void mainwindow::on_scale_change()
{
	std::cout << "Value updated" << std::endl;
}
