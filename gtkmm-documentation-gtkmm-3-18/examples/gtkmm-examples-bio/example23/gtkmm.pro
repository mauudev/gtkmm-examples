# Disable Qt core and Qt Graphical user interface (don't use Qt)
QT              -= core
QT              -= gui

# Name of the target (executable file)
TARGET          = gtkmm

# This is a console application
CONFIG          += console
CONFIG          -= app_bundle

# This is an application
TEMPLATE        = app

# Sources files
SOURCES         += main.cpp \
    scrollimage.cpp

# GTK+ library
unix: CONFIG	+= link_pkgconfig
unix: PKGCONFIG += gtkmm-3.0

HEADERS += \
    scrollimage.h
