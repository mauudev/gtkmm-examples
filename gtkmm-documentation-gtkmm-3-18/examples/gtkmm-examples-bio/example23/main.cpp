#include "scrollimage.h"
#include <gtkmm.h>


int main(int argc, char* argv[])
{
	// Initialize gtkmm and create the main window
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "www.lucidarme.me");
	Gtk::Window window;

	// Create the drawing and add an image from file
	ScrollImage Img;
	Img.set_image_from_file("gtk.png");

	// Insert the drawing in the window
	window.add(Img);
	// Resize the window
	window.resize(600,400);
	// Set the window title
	window.set_title("ScrollImage");
	// Show the drawing
	Img.show();

	// Start main loop
	return app->run(window);
}
