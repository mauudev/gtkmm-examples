#ifndef SCROLLIMAGE_H
#define SCROLLIMAGE_H

#include <gtkmm.h>
#include <iostream>


// Minimun scale (low boundary)
#define			MIN_SCALE			0.05



class ScrollImage : public Gtk::DrawingArea
{
public:


	/*!
	 * \brief ScrollImage				Constructor, initialize the widget.
	 */
	ScrollImage();


	/*!
	 * \brief set_image_from_file		Load the image to display.
	 * \param filename					File name of the image.
	 */
	void set_image_from_file(const std::string& filename);





protected:

	/*!
	 * \brief on_draw					Override the draw handler : update display.
	 *									Don't call this function to update display, call queue_draw().
	 * \param cr						Cairo context.
	 * \return							Always false.
	 */
	virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);


	/*!
	 * \brief displayTarget				Display a little target at coordinates x,y.
	 * \param cr						Cairo context.
	 * \param x							x-coordinate of the center of the target.
	 * \param y							y-coordinate of the center of the target.
	 */
	void displayTarget(const Cairo::RefPtr<Cairo::Context>& cr,double x, double y);


	/*!
	 * \brief on_scroll_event			This function is called when the mouse scroll changes.
	 *									This is a default handler for the signal signal_scroll_event().
	 * \param event						Properties of the event.
	 * \return							Always true (the event is processed).
	 */
	bool on_scroll_event(GdkEventScroll *event);


	/*!
	 * \brief on_button_press_event		This function is called when a mouse button is pressed.
	 *									This is a default handler for the signal signal_button_press_event().
	 * \param event						Properties of the event.
	 * \return							True if the event is processed, false otherwise.
	 */
	bool on_button_press_event(GdkEventButton *event);


	/*!
	 * \brief on_button_release_event	This function is called when a mouse button is released
	 *									This is a default handler for the signal signal_button_release_event().
	 * \param event						Properties of the event.
	 * \return							True if the event is processed, false otherwise.
	 */
	bool on_button_release_event(GdkEventButton *event);


	/*!
	 * \brief on_motion_notify_event	This function is called when the pointer is moved
	 *									This is a default handler for the signal signal_motion_notify_event().
	 * \param event						Properties of the event.
	 * \return							True if the event is processed, false otherwise.
	 */
	bool on_motion_notify_event(GdkEventMotion*event);



protected:
	/*!
	 * \brief resetView					Reset the view (best image fit in the window).
	 * \param winWidth					Width of the window (in pixels).
	 * \param winHeight					Height of the window (in pixels).
	 * \param imgWidth					Width of the image (in pixels).
	 * \param imgHeight					Height of the image (in pixels).
	 */
	void                        fitImage(int winWidth,int winHeight, int imgWidth, int imgHeight);


	/*!
	 * \brief fit						Set the fit flag to one and update display
	 *									Next call of on_draw will fit the image according to the drawing area.
	 */
	void                        fit();


	/*!
	 * \brief reset						Reset display (center the image and set scale to 1:1) and update display
	 */
	void                        reset();


	/*!
	 * \brief resetScale				Set scale to 1:1 and update display
	 */
	void                        resetScale();


	/*!
	 * \brief setCenter					Center the image on the pointer position from last click
	 */
	void                        setCenter();

	/*!
	 * \brief showHideTarget			Show or hide the center target and update display
	 */
	void                        showHideTarget();


private:

	// Image to display
	Glib::RefPtr<Gdk::Pixbuf>   image;

	// Scale of the image
	double                      scale;

	// Coordinates of the image point to place at the center of the window (focussed pixel)
	double                      imgFocusX,imgFocusY;

	// Used to memorize last mouse coordinates
	int                         lastXMouse, lastYMouse;

	// Flags
	bool                        resetFlag;
	bool                        moveFlag;
	bool                        targetFlag;

	// Popup menu and submenus

	// Main popup menu
	Gtk::Menu                   m_Menu_Popup;
	Gtk::MenuItem               MenuItemFit;
	Gtk::MenuItem               MenuItemRstView;
	Gtk::MenuItem               MenuItemRstScale;
	Gtk::MenuItem               MenuItemSetCenter;
	Gtk::MenuItem               MenuItemTarget;
	Gtk::SeparatorMenuItem      MenuItemLine1;
	Gtk::SeparatorMenuItem      MenuItemLine2;

};

#endif // SCROLLIMAGE_H
