#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <gtkmm.h>




// The class mainwindow inherits from Gtk::Window
class mainwindow : public Gtk::Window
{
	// Constructor and destructor
public:    
	mainwindow();
	virtual                 ~mainwindow();

protected:
	void                    messageInfo();
	void                    messageWarning();
	void                    messageQuestion();
	void                    messageError();
	void                    messageOther();

	//Override default signal handler:
	//    virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

	//Member widgets:
	Gtk::Button             buttonInfo;
	Gtk::Button             buttonWarning;
	Gtk::Button             buttonQuestion;
	Gtk::Button             buttonError;
	Gtk::Button             buttonOther;
	Gtk::Button             buttonQuit;
	Gtk::VBox               mainLayout;

};


#endif // MAINWINDOW_H
