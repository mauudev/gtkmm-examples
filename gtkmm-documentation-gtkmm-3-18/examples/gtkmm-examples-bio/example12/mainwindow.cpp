#include "mainwindow.h"
#include <iostream>


// Constructor of the main Window (build ui interface).
mainwindow::mainwindow()
{
	this->resize(400,200);

	// Add the info message button
	buttonInfo.add_label("Information");
	buttonInfo.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::messageInfo));
	mainLayout.pack_start(buttonInfo);

	// Add the warning message button
	buttonWarning.add_label("Warning");
	buttonWarning.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::messageWarning));
	mainLayout.pack_start(buttonWarning);

	// Add the question message button
	buttonQuestion.add_label("Question");
	buttonQuestion.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::messageQuestion));
	mainLayout.pack_start(buttonQuestion);

	// Add the error message button
	buttonError.add_label("Error");
	buttonError.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::messageError));
	mainLayout.pack_start(buttonError);

	// Add the other message button
	buttonOther.add_label("Other");
	buttonOther.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::messageOther));
	mainLayout.pack_start(buttonOther);

	// Add the Quit button
	buttonQuit.add_label("Quit");
	buttonQuit.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::close));
	mainLayout.pack_start(buttonQuit);

	// Display the main grid in the main window
	mainLayout.show_all();

	// Insert the grid in the main window
	add(mainLayout);

}


// Destructor of the class
mainwindow::~mainwindow()
{}


// Display message info
void mainwindow::messageInfo()
{
	Gtk::MessageDialog dialog(*this, "This is an information MessageDialog",false,Gtk::MESSAGE_INFO);
	dialog.set_secondary_text("And this is the secondary text that explains things.");
	dialog.run();
}

// Display warning message
void mainwindow::messageWarning()
{
	Gtk::MessageDialog dialog(*this, "This is a warning MessageDialog",false,Gtk::MESSAGE_WARNING);
	dialog.run();
}

// Display a question message
void mainwindow::messageQuestion()
{
	Gtk::MessageDialog dialog(*this, "This is a question MessageDialog",false,Gtk::MESSAGE_QUESTION,Gtk::BUTTONS_OK_CANCEL);
	int Answer=dialog.run();

	// Process user choice
	switch(Answer)
	{
	case(Gtk::RESPONSE_OK):
		std::cout << "OK clicked." << std::endl;
		break;
	case(Gtk::RESPONSE_CANCEL):
		std::cout << "Cancel clicked." << std::endl;
		break;
	default:
		std::cout << "Unexpected button clicked." << std::endl;
		break;
	}
}

// Display error message
void mainwindow::messageError()
{
	Gtk::MessageDialog dialog(*this, "This is an error MessageDialog",false,Gtk::MESSAGE_ERROR);
	dialog.run();
}

// Display other message
void mainwindow::messageOther()
{
	Gtk::MessageDialog dialog(*this, "<u>This is an</u> <b>other</b> <i>MessageDialog</i>",true,Gtk::MESSAGE_OTHER);
	dialog.set_secondary_text("Note that the text support <b>HTML</b> markups.",true);
	dialog.run();
}
