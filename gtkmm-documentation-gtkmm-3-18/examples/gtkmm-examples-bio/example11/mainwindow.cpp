#include "mainwindow.h"
#include <iostream>


// Constructor of the main Window (build ui interface).
mainwindow::mainwindow()
{
	// Initialize seconds
	seconds=0;

	// Add the label displaying the ellapsed tim
	label.set_text("Elapsed time: 0 s");
	mainGrid.attach(label,0,0,1,1);

	// Add the Quit button
	buttonQuit.add_label("Quit");
	buttonQuit.set_size_request(200,50);
	buttonQuit.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::close));
	mainGrid.attach(buttonQuit,0,1,1,1);

	// Display the main grid in the main window
	mainGrid.show_all();

	// Launch timer every second
	Glib::signal_timeout().connect( sigc::mem_fun(*this, &mainwindow::on_my_timeout), 1000 );

	// Insert the grid in the main window
	add(mainGrid);
}


// Destructor of the class
mainwindow::~mainwindow()
{}


bool mainwindow::on_my_timeout()
{
	// Prepare message
	char Text[50];
	sprintf (Text,"Elapsed time: %d s",++seconds);
	// Display message in the label
	label.set_text(Text);
	return true;
}
