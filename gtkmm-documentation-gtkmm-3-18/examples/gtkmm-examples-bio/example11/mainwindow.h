#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <gtkmm.h>
#include <gtkmm/drawingarea.h>
#include <gtkmm/application.h>
#include <gtkmm/window.h>


// The class mainwindow inherits from Gtk::Window
class mainwindow : public Gtk::Window
{
	// Constructor and destructor
public:    
	mainwindow();
	virtual                 ~mainwindow();

protected:
	// Called every timeout
	bool                    on_my_timeout();

	//Member widgets:
	Gtk::Label              label;
	Gtk::Button             buttonQuit;
	Gtk::Grid               mainGrid;

private:
	int                     seconds;

};


#endif // MAINWINDOW_H
