#include "mainwindow.h"
#include <stdlib.h>
#include <iostream>


// Constructor of the main Window (build ui interface).
mainwindow::mainwindow()
{
    // Initialize the main window
    this->set_title("Spin button");
    this->set_border_width(10);

    // Add a label for the spin button
    label.set_text("X [0;600] : ");
    label.set_size_request(10,10);
    label.set_alignment(Gtk::ALIGN_END);
    mainGrid.attach(label,0,0,1,1);

    // Add the spin button
    spin.set_range(0,600);
    spin.set_value(300);
    spin.set_increments(10,10);
    mainGrid.attach(spin,1,0,1,1);

    // Create the first button
    button1.add_label("Move window");
    button1.set_size_request(100,30);
    button1.signal_clicked().connect(sigc::mem_fun(*this,&mainwindow::on_button));
    mainGrid.attach(button1,2,0,1,1);


    // Add the Quit button
    buttonQuit.add_label("Quit");
    buttonQuit.set_size_request(300,50);
    buttonQuit.signal_clicked().connect(sigc::mem_fun(*this,&mainwindow::close));
    mainGrid.attach(buttonQuit,0,1,3,1);

    // Display the main grid in the main window
    mainGrid.set_column_spacing(10);
    mainGrid.set_row_spacing(10);
    mainGrid.show_all();
    this->on_button();
    add(mainGrid);
}


// Destructor of the class
mainwindow::~mainwindow()
{}


// Call when the button is clicked
void mainwindow::on_button()
{
    std::cout << "New window position is : " << spin.get_value() << ",100" << std::endl;
    this->move(spin.get_value(),100);
}
