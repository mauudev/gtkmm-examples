#include "mainwindow.h"
#include <gtkmm.h>


int main(int argc, char* argv[])
{
    Gtk::Main app(argc, argv);
    mainwindow w;
    Gtk::Main::run(w);
    return 0;
}
