#-------------------------------------------------
#
# Project created by QtCreator 2014-12-07T07:06:41
#
#-------------------------------------------------

QT       -= core

QT       -= gui

TARGET = gtkmm
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
SOURCES += drawing.cpp

HEADERS += drawing.h

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += gtkmm-3.0

