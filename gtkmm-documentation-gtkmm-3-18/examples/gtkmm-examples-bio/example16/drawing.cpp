#include "drawing.h"

Drawing::Drawing()
{
	// Load the image
	image = Gdk::Pixbuf::create_from_file("gtk.png");
}


bool Drawing::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{

	// Place the image at 0,0
	Gdk::Cairo::set_source_pixbuf(cr, image, 0,0);
	// Update the area where the image is located
	cr->rectangle(0, 0, image->get_width(), image->get_height());
	// Fill the area with the image
	cr->fill();

	return true;
}
