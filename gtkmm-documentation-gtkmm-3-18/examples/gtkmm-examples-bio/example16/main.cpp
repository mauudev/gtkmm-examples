#include "drawing.h"
#include <gtkmm.h>


int main(int argc, char* argv[])
{
	// Initialize gtkmm and create the main window
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "www.lucidarme.me");
	Gtk::Window window;

	// Create the drawing
	Drawing Dwg;

	// Insert the drawing in the window
	window.add(Dwg);

	// Show the drawing
	Dwg.show();

	// Start main loop
	return app->run(window);
}

