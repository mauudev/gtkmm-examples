#include "drawing.h"

Drawing::Drawing()
{
    // Load the image
    image = Gdk::Pixbuf::create_from_file("gtk.png");
    add_events(Gdk::BUTTON_PRESS_MASK | Gdk::SCROLL_MASK  |Gdk::SMOOTH_SCROLL_MASK);
    scale=1;
}


bool Drawing::on_scroll_event(GdkEventScroll *ev)
{
    // Update scale according to mouse scroll
    scale-=ev->delta_y/10.;
	if (scale<0.1) scale=0.1;
    std::cout << scale << std::endl;
    std::cout.flush();
    // Update drawing
    queue_draw();
    // There is probably a good reason to do this
    return true;
}




bool Drawing::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    // Get drawing area size
    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    // Scale the image to the area
    cr->scale(scale,scale);

    // Place the image at the center of the window
    Gdk::Cairo::set_source_pixbuf(cr, image, (width/2)/scale-image->get_width()/2,(height/2)/scale-image->get_height()/2);

    // Update the whole drawing area
    cr->rectangle(0, 0, get_allocation().get_width()/scale, get_allocation().get_width()/scale);
    // Fill the area with the image
    cr->fill();

    return true;
}
