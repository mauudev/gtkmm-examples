#ifndef DRAWING_H
#define DRAWING_H

#include <iostream>
#include <gtkmm.h>

class Drawing : public Gtk::DrawingArea
{
public:
    Drawing();

protected:
    // Override default signal handler:
    virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

    // Override mouse scroll event
    bool on_scroll_event(GdkEventScroll *ev);



private:
    // Image displayed
    Glib::RefPtr<Gdk::Pixbuf>   image;
    // Scale of the image
    double                      scale;


};

#endif // DRAWING_H
