#include "drawing.h"
#include <gtkmm.h>


int main(int argc, char* argv[])
{
	// Initialize gtkmm and create the main window
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "www.lucidarme.me");
	Gtk::Window window;

	// Create the drawing
	Drawing Dwg;


	// Insert the drawing in the window
	window.add(Dwg);
	// Resize the window
	window.resize(400,400);
	// Set the window title
	window.set_title("Mouse scroll");
	// Show the drawing
	Dwg.show();

	// Start main loop
	return app->run(window);
}

