#include <gtkmm.h>

int main(int argc, char* argv[])
{
    // Initialize gtkmm and create the main window
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "www.lucidarme.me");

    // Create the main window with an image
    Gtk::Window window;
    Gtk::Image image1("gtk.png");
    window.add(image1);
    image1.show();
    window.move(200,200);
    window.set_title("Main window");

    // Create the secondary window with another image
    Gtk::Window window2;
    Gtk::Image image2("minirex.png");
    window2.add(image2);
    image2.show();
    window2.set_title("Secondary window");
    window2.show();
    window2.set_deletable(false);
    window2.move(400,200);

    // Start main loop
    return app->run(window);
}

