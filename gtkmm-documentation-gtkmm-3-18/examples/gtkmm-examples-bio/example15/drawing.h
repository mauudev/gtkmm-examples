#ifndef DRAWING_H
#define DRAWING_H

#include <iostream>
#include <gtkmm.h>

class Drawing : public Gtk::DrawingArea
{
public:
	Drawing();

protected:
	//Override default signal handler:
	virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

};

#endif // DRAWING_H
