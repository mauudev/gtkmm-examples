#include "drawing.h"

Drawing::Drawing()
{
}


bool Drawing::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
	// Draw the first circle
	cr->arc(75,75,50,0,2*M_PI);
	cr->set_source_rgba(0, 0, 1.0,0.5);
	cr->fill();
	cr->stroke();

	cr->set_source_rgba(0, 1, 0,0.5);
	cr->arc(100,125,50,0,2*M_PI);
	cr->fill();
	cr->stroke();

	cr->set_source_rgba(1, 0, 0,0.5);
	cr->arc(125,75,50,0,2*M_PI);
	cr->fill();
	cr->stroke();


	return true;
}
