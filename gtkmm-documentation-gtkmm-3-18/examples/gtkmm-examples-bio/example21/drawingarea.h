#ifndef DRAWINGAREA_H
#define DRAWINGAREA_H

#include <gtkmm.h>
#include <iostream>


class DrawingArea : public Gtk::DrawingArea
{
public:
	DrawingArea();

protected:
	// Override default signal handler:
	virtual bool                on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

	// Override mouse events
	bool                        on_button_press_event(GdkEventButton *event);

	// Signal handlers (run when a popup item is clicked)
	void                        on_action1_event();
	void                        on_action2_event();

private:

	// Image displayed
	Glib::RefPtr<Gdk::Pixbuf>   image;

	// Scale of the image
	double                      scale;

	// Popup menu and submenus
	Gtk::Menu                   m_Menu_Popup;
	Gtk::MenuItem               MenuItem1;
	Gtk::MenuItem               MenuItem2;

};
#endif // DRAWINGAREA_H
