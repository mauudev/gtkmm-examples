#include "drawingarea.h"



DrawingArea::DrawingArea()
{
	// Load the image
	image = Gdk::Pixbuf::create_from_file("gtk.png");

	// Set masks for mouse events
	add_events(Gdk::BUTTON_PRESS_MASK);

	// ::: Create popup menu :::

	// Add and connect action 1
	MenuItem1.set_label("Action 1");
	MenuItem1.signal_activate().connect(sigc::mem_fun(*this,&DrawingArea::on_action1_event));
	m_Menu_Popup.append(MenuItem1);

	// Add and connect action 2
	MenuItem2.set_label("Action 2");
	MenuItem2.signal_activate().connect(sigc::mem_fun(*this,&DrawingArea::on_action2_event));
	m_Menu_Popup.append(MenuItem2);

	// Show the menu
	m_Menu_Popup.show_all();

	// Connect the menu to this Widget
	m_Menu_Popup.accelerate(*this);
}


// Signal handlers for acion 1
void DrawingArea::on_action1_event()
{
	std::cout << "Action 1 selected" << std::endl;
}


// Signal handlers for acion 2
void DrawingArea::on_action2_event()
{
	std::cout << "Action 2 selected" << std::endl;
}


// Mouse button press event
bool DrawingArea::on_button_press_event(GdkEventButton *event)
{
	// Check if the event is a right button click.
	if( (event->type == GDK_BUTTON_PRESS) && (event->button == 3) )
	{
		// Display the popup menu
		m_Menu_Popup.popup(event->button, event->time);
		// The event has been handled.
		return true;
	}

	// Propagate the event further.
	return false;
}


// Call when the display need to be updated
bool DrawingArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
	// Place the image at the center of the window
	Gdk::Cairo::set_source_pixbuf(cr, image, 0,0);
	// Update the whole drawing area
	cr->rectangle(0, 0, image->get_width(), image->get_height());
	// Fill the area with the image
	cr->fill();
	// The event has been handled.
	return true;
}
