#include "mainwindow.h"
#include <iostream>


// Constructor of the main Window (build ui interface).
mainwindow::mainwindow()
{
	// Initialize the main window and hide the title bar
	this->set_border_width(10);

	// Add the Quit button
	buttonQuit.add_label("Quit");
	buttonQuit.set_size_request(200,50);
	buttonQuit.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::close));
	mainGrid.attach(buttonQuit,0,1,1,1);

	// Display the main grid in the main window
	mainGrid.show_all();

	// Insert the grid in the main window
	add(mainGrid);

	this->set_icon_from_file("gtk_icon.png");
}


// Destructor of the class
mainwindow::~mainwindow()
{}
