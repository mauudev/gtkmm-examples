#include "mainwindow.h"
#include <iostream>


// Constructor of the main Window (build ui interface).
mainwindow::mainwindow()
{
    // Initialize the main window and hide the title bar
    this->set_border_width(10);

    // Load and display the image
    image.set("gtk.png");
    mainGrid.attach(image,0,0,2,1);

    // Add the button for the first image
    buttonImage1.add_label("GTK");
    buttonImage1.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::displayImage1));
    mainGrid.attach(buttonImage1,0,1,1,1);

    // Add the button for the second image
    buttonImage2.add_label("MiniRex");
    buttonImage2.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::displayImage2));
    mainGrid.attach(buttonImage2,1,1,1,1);

    // Add the Quit button
    buttonQuit.add_label("Quit");
    buttonQuit.signal_pressed().connect(sigc::mem_fun(*this,&mainwindow::close));
    mainGrid.attach(buttonQuit,0,2,2,1);

    // Display the main grid in the main window
    mainGrid.show_all();

    // Insert the grid in the main window
    add(mainGrid);
}


// Destructor of the class
mainwindow::~mainwindow()
{}

// Display the first image
void mainwindow::displayImage1()
{
    image.set("gtk.png");
}

// Display the second image
void mainwindow::displayImage2()
{
    image.set("minirex.png");
}
