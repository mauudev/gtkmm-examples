#include "drawing.h"

Drawing::Drawing()
{
}


bool Drawing::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    // Set color
    cr->set_source_rgb(0, 0.0, 1.0);
    // Set line width
    cr->set_line_width(15.0);
    // Set line cap
    cr->set_line_cap(Cairo::LINE_CAP_ROUND);

    // Draw the lines
    cr->move_to(50, 50);
    cr->line_to(150, 150);
    cr->move_to(150,50);
    cr->line_to(50, 150);

    // Apply drawing
    cr->stroke();

    return true;
}
