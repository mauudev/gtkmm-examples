#ifndef REMOVE_ELEMENT_EXAMPLE_H
#define REMOVE_ELEMENT_EXAMPLE_H

#include <gtkmm.h>
#include "GenericButton.h"

class ExampleButtonBox : public Gtk::Frame
{
public:
    ExampleButtonBox(bool horizontal,
        const Glib::ustring &title,
        gint spacing,
        Gtk::ButtonBoxStyle layout);

    void on_click_remove_btn(Gtk::ButtonBox*);

    void removeElement(vector<GenericButton*>,
        Gtk::ButtonBox*,
        int i);

protected:
    Gtk::Button m_buttonRemove;
    Gtk::ButtonBox* m_buttonBox; 
    vector<GenericButton*> m_btnList;
};
#endif 
