#include "RemoveElementExample.h"
#include <iostream>
#include <unistd.h>

using namespace std;

ExampleButtonBox::ExampleButtonBox(bool horizontal,
    const Glib::ustring &title,
    gint spacing,
    Gtk::ButtonBoxStyle layout)
: Gtk::Frame(title),
    m_buttonRemove("Delete")
{
    m_buttonBox = nullptr;

    if(horizontal)
        m_buttonBox = Gtk::manage( new Gtk::ButtonBox(
            Gtk::ORIENTATION_HORIZONTAL) );
    else
        m_buttonBox = Gtk::manage( new Gtk::ButtonBox(
            Gtk::ORIENTATION_VERTICAL) );

    m_buttonBox->set_border_width(5);
    add(*m_buttonBox);

    m_buttonBox->set_layout(layout);
    m_buttonBox->set_spacing(spacing);
    m_buttonBox->add(m_buttonRemove);

    m_buttonRemove.signal_clicked().connect(
        sigc::bind<Gtk::ButtonBox*>(sigc::mem_fun(*this, 
            &ExampleButtonBox::on_click_remove_btn), m_buttonBox));

    for(int i = 1; i < 5; i++){
        GenericButton *btn = new GenericButton(i,"Test Btn " + 
            std::to_string(i));
        m_btnList.push_back(btn);
        m_buttonBox->add(*btn);
    }
}

void ExampleButtonBox::on_click_remove_btn(Gtk::ButtonBox *p_buttonBox)
{
    int i = 0;
    while(i < m_btnList.size())
    {
        if(m_btnList[i]->get_active()){
            p_buttonBox->remove(*m_btnList[i]);
            m_btnList.erase(m_btnList.begin()+i);
            i = 0;
            continue;
        }
        cout << i << endl;
        i++; 
    }
    cout << "Button " << " removed !" << " array size: " 
        << m_btnList.size() << "\n";
}

void ExampleButtonBox::removeElement(vector<GenericButton*> list, 
    Gtk::ButtonBox *p_buttonBox, int i)
{
    p_buttonBox->remove(*list[i]);
    list.erase(list.begin()+i);
}
