#ifndef TOOLDEFINITION_H
#define TOOLDEFINITION_H

#include <string>
#include <gtkmm.h>
using namespace std;

class ToolDefinition : public Gtk::ToggleButton
{
    string m_typeName;
    string m_toolTip;
    string m_filepath;
    int m_id;
    vector<string> m_events;
    Gtk::Image m_image;
    Glib::RefPtr<Gdk::Pixbuf> m_imageResource;

public:
    ToolDefinition();
    ToolDefinition(string m_typeName, string m_toolTip, int m_id);
    virtual ~ToolDefinition();

    string getTypeName()const;
    string getFilepath()const;
    string getToolTip()const;
    int getId()const;
    vector<string>& getEventsVector();
    Gtk::Image& getImage(); 
};
#endif