#ifndef AGENT_H
#define AGENT_H

#include <string>
#include "ToolDefinition.h"
using namespace std;

class Agent : public ToolDefinition
{
    string name;
    string tooltip;
    int id;

public:
    Agent();
    Agent(string name,string tooltip,int id);
    virtual ~Agent();

    string getName()const;
    int getId()const;
};

#endif