#ifndef GENERIC_BUTTON_H
#define GENERIC_BUTTON_H

#include <gtkmm.h>
#include <string>

using namespace std;

class GenericButton : public Gtk::ToggleButton
{
    int m_buttonId;
    string m_label;
public:
    GenericButton(int p_buttonId, 
        string p_label);
    virtual ~GenericButton();

    int getButtonId();

};
#endif