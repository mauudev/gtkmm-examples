#include "RemoveElementExample.h"
#include "Agent.h"
#include <iostream>
#include <unistd.h>

using namespace std;

ExampleButtonBox::ExampleButtonBox(bool horizontal,
    const Glib::ustring &title,
    gint spacing,
    Gtk::ButtonBoxStyle layout)
: Gtk::Frame(title)
{
    m_buttonBox = nullptr;

    if(horizontal)
        m_buttonBox = Gtk::manage( new Gtk::ButtonBox(
            Gtk::ORIENTATION_HORIZONTAL) );
    else
        m_buttonBox = Gtk::manage( new Gtk::ButtonBox(
            Gtk::ORIENTATION_VERTICAL) );

    m_buttonBox->set_border_width(5);
    add(*m_buttonBox);

    m_buttonBox->set_layout(layout);
    m_buttonBox->set_spacing(spacing);

    signal_key_press_event().connect(sigc::mem_fun(*this, 
            &ExampleButtonBox::windowEventKeyPress));

    for(int i = 1; i < 5; i++)
    {
        string s1 = "chrome";
        string s2 = "Test Btn !";
        Agent *btn = new Agent(s1,s2,i);
        m_btnList.push_back(btn);
        m_buttonBox->add(*btn);
    }
}

void ExampleButtonBox::removeActiveElements()
{
    int i = 0;
    while(i < m_btnList.size())
    {
        if(m_btnList[i]->get_active())
        {
            m_buttonBox->remove(*m_btnList[i]);
            m_btnList.erase(m_btnList.begin()+i);
            i = 0;
            continue;
        }
        cout << i << endl;
        i++; 
    }
    cout << "Button " << " removed !" << " array size: " << 
        m_btnList.size() << "\n";
}
bool ExampleButtonBox::windowEventKeyPress(GdkEventKey *p_event)
{
    m_eventHandlerFlag = false;

    if (p_event->type == GDK_KEY_PRESS && p_event->keyval == GDK_KEY_Delete)
    {
        m_eventHandlerFlag = true;
    }
    if(m_eventHandlerFlag)
    {
        removeActiveElements();
    }
    std::cout << "Key pressed: " << p_event->hardware_keycode
        << "\n";
    return false;
}
