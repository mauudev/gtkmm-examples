#include "ToolDefinition.h"
#include <iostream>     

using namespace std;

ToolDefinition::ToolDefinition(string name, string tooltip, 
    int id):Gtk::ToggleButton(tooltip),
    m_typeName{name},
    m_id{id},
    m_toolTip{tooltip}
{
    m_filepath = "../resources/"+m_typeName + ".png";
    try{
        m_imageResource = Gdk::Pixbuf::create_from_file(m_filepath,100,100,
            true);
        m_image.set(m_imageResource);
        this->set_image(m_image);
    }
    catch(...) 
    {
        cout << "An error occurred during image loading: " << m_filepath << endl;
    }
}

ToolDefinition::ToolDefinition()
{

}

ToolDefinition::~ToolDefinition()
{

}

string ToolDefinition::getFilepath()const
{
    return m_filepath;
}

string ToolDefinition::getTypeName()const
{
    return m_typeName;
}

int ToolDefinition::getId()const
{
    return m_id;
}

vector<string>& ToolDefinition::getEventsVector()
{
    return m_events;
}

Gtk::Image& ToolDefinition::getImage()
{
    return m_image;
}