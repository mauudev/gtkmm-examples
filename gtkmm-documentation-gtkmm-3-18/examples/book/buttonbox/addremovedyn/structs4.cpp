struct X
{
	virtual void doSomething() = 0;	
};

struct Y : X{};
struct Z : X{};

struct ZZZ: Y, Z
{
	void doSomething() override
	{
		puts("Hello from dev26 !");
	}
};

int main()
{
	ZZZ z;
	z.doSomething();
}