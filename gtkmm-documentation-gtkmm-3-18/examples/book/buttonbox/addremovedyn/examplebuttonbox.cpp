//$Id: examplebuttonbox.cc 836 2007-05-09 03:02:38Z jjongsma $ -*- c++ -*-

/* gtkmm example Copyright (C) 2002 gtkmm development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "examplebuttonbox.h"
#include <iostream>
using namespace std;

ExampleButtonBox::ExampleButtonBox(bool horizontal,
       const Glib::ustring& title,
       gint spacing,
       Gtk::ButtonBoxStyle layout)
: Gtk::Frame(title),
  m_Button_Add("Add new"),
  m_Button_Remove("Remove all"),
  m_Button_Help("Help")
{
  Gtk::ButtonBox* bbox = nullptr;
  Gtk::Button*  m_Button_Remove_ref = &m_Button_Remove;
  if(horizontal)
    bbox = Gtk::manage( new Gtk::ButtonBox(Gtk::ORIENTATION_HORIZONTAL) );
  else
    bbox = Gtk::manage( new Gtk::ButtonBox(Gtk::ORIENTATION_VERTICAL) );

  bbox->set_border_width(5);

  add(*bbox);

  /* Set the appearance of the Button Box */
  bbox->set_layout(layout);
  bbox->set_spacing(spacing);

  bbox->add(m_Button_Add);
  bbox->add(m_Button_Remove);
  bbox->add(m_Button_Help);

  //m_Button_Add.signal_clicked().connect(sigc::mem_fun(*this, &ExampleButtonBox::on_click_button_add));
  m_Button_Add.signal_clicked().connect( sigc::bind<Gtk::ButtonBox*,Gtk::Button*>( sigc::mem_fun(*this, &ExampleButtonBox::on_click_button_add), bbox, m_Button_Remove_ref) );
  //bbox->remove(m_Button_Add);  
}

void ExampleButtonBox::on_click_button_add(Gtk::ButtonBox* bbox, Gtk::Button* button)
{
   cout << "click on add button" << "\n";
   bbox->remove(*button); 
}

