#include <iostream>
#include "GenericButton.h"
#include <string>

using namespace std;

GenericButton::GenericButton(int button_id, string label):Gtk::Button(label),button_id{button_id}{}

GenericButton::~GenericButton(){}

int GenericButton::get_button_id(){return button_id;}
