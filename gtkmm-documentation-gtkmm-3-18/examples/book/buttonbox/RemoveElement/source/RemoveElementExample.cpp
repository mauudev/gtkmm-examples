//$Id: examplebuttonbox.cc 836 2007-05-09 03:02:38Z jjongsma $ -*- c++ -*-

/* gtkmm example Copyright (C) 2002 gtkmm development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "RemoveElementExample.h"
#include <iostream>
using namespace std;

ExampleButtonBox::ExampleButtonBox(bool horizontal,
       const Glib::ustring& title,
       gint spacing,
       Gtk::ButtonBoxStyle layout)
: Gtk::Frame(title),
  m_Button_Add("Remove Btn 1"),
  m_Button_Remove("Btn 1"),
  m_Button_Help("Something")
{
  Gtk::ButtonBox* bbox = nullptr;
  Gtk::Button*  m_Button_Remove_ref = &m_Button_Remove;

  Gtk::Menu* m_pMenuPopup1;

  if(horizontal)
    bbox = Gtk::manage( new Gtk::ButtonBox(Gtk::ORIENTATION_HORIZONTAL) );
  else
    bbox = Gtk::manage( new Gtk::ButtonBox(Gtk::ORIENTATION_VERTICAL) );

  bbox->set_border_width(5);

  add(*bbox);

  //GenericButton *testBtn = new GenericButton(1,"Hola putito !");
  /* Set the appearance of the Button Box */
  bbox->set_layout(layout);
  bbox->set_spacing(spacing);


  for(int i = 1; i < 10; i++){
    GenericButton *btn = new GenericButton(i,"Test Btn " + std::to_string(i));
    btn_list.push_back(btn);

    // btn->signal_clicked().connect(sigc::bind<Gtk::ButtonBox*, GenericButton*>(
    //   sigc::mem_fun(*this, &ExampleButtonBox::on_button_clicked), bbox, btn));

    bbox->add(*btn);
  }
  m_pMenuPopup = new Gtk::Menu();

  auto item = Gtk::manage(new Gtk::MenuItem("_Edit", true));
  item->signal_activate().connect(
    sigc::mem_fun(*this, &ExampleButtonBox::on_menu_file_popup_generic) );
  m_pMenuPopup->append(*item);
    
  item = Gtk::manage(new Gtk::MenuItem("_Process", true));
  item->signal_activate().connect(
    sigc::mem_fun(*this, &ExampleButtonBox::on_menu_file_popup_generic) );
  m_pMenuPopup->append(*item);
    
  item = Gtk::manage(new Gtk::MenuItem("_Remove", true));
  item->signal_activate().connect(
    sigc::mem_fun(*this, &ExampleButtonBox::on_menu_file_popup_generic) );
  m_pMenuPopup->append(*item);


  m_pMenuPopup->accelerate(*this);
  m_pMenuPopup->show_all(); //Show all menu items when the menu pops up

}

GenericButton& ExampleButtonBox::on_button_clicked(Gtk::ButtonBox*, GenericButton*)
{

}

void ExampleButtonBox::on_click_remove_btn(Gtk::ButtonBox* bbox, GenericButton* btn)
{
  cout << "Button whith label: " << "'" << btn->get_label() << "'" << " with id: " << btn->get_button_id() << " removed !" << "\n";
  bbox->remove(*btn); 
}

void ExampleButtonBox::on_menu_file_popup_generic()
{
  auto refSelection = get_focus_child();
    
  std::cout << "Removing " << refSelection << std::endl;
}

bool ExampleButtonBox::on_button_press_event(GdkEventButton* button_event)
{
  bool return_value = false;

  //Then do our custom stuff:
  if( (button_event->type == GDK_BUTTON_PRESS) && (button_event->button == 3) )
  {
    m_pMenuPopup->popup(button_event->button, button_event->time);
  }

  return return_value;
}
