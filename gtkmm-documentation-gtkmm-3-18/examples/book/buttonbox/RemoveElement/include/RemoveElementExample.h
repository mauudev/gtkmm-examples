//$Id: examplebuttonbox.h 705 2006-07-19 02:55:32Z jjongsma $ -*- c++ -*-

/* gtkmm example Copyright (C) 2002 gtkmm development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef REMOVE_ELEMENT_EXAMPLE_H
#define REMOVE_ELEMENT_EXAMPLE_H

#include <gtkmm.h>
#include "GenericButton.h"

class ExampleButtonBox : public Gtk::Frame
{
public:
  ExampleButtonBox(bool horizontal,
       const Glib::ustring& title,
       gint spacing,
       Gtk::ButtonBoxStyle layout);

  void on_click_remove_btn(Gtk::ButtonBox*, GenericButton*);

  bool on_button_press_event(GdkEventButton* button_event) override;
  
  void on_menu_file_popup_generic();

  GenericButton& on_button_clicked(Gtk::ButtonBox*, GenericButton*);

protected:
  Gtk::Button m_Button_Add, m_Button_Remove, m_Button_Help;
  Gtk::ButtonBox bbox; 
  vector<GenericButton*> btn_list;

  GenericButton* btn_ref;

  Glib::RefPtr<Gtk::Builder> m_refBuilder;

  Gtk::Menu* m_pMenuPopup;
};

#endif 
