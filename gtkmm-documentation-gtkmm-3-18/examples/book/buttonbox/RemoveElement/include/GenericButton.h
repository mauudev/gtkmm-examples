#ifndef GENERIC_BUTTON_H
#define GENERIC_BUTTON_H

#include <gtkmm.h>
#include <string>

using namespace std;

class GenericButton : public Gtk::Button
{
	int button_id;
	string label;
public:
	GenericButton(int button_id, string label);
	~GenericButton();

	int get_button_id();

};
#endif