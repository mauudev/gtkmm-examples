#ifndef TOOL_DEFINITION_H
#define TOOL_DEFINITION_H

#include <string>
#include <gtkmm.h>
using namespace std;

class ToolDefinition
{
    string m_name;
    size_t m_id;
    vector<int> m_events;
    Gtk::Image m_image;

public:
    ToolDefinition();
    ~ToolDefinition(); 
    string getName()
    {
        return m_name;
    }

    size_t getId()
    {
        return m_id;
    }

    vector<int>& getEventsArray()
    {
        return m_events;
    }  
    
}
#endif