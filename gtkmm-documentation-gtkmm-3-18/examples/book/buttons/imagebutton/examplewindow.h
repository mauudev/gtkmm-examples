#ifndef EXAMPLE_WINDOW_H
#define EXAMPLE_WINDOW_H

#include <gtkmm.h>
class ExampleWindow : public Gtk::Window
{
public:
    ExampleWindow();
    virtual ~ExampleWindow();

protected:
    void on_clicked_button();

    Gtk::ToggleButton m_toggleButton;
    Gtk::Image m_image;
    Glib::RefPtr<Gdk::Pixbuf> m_imageResource;
};
#endif