#include "examplewindow.h"
#include <iostream>

using namespace std;

ExampleWindow::ExampleWindow()
{
    m_imageResource = Gdk::Pixbuf::create_from_file("pacman.png",100,100,true);
    m_image.set(m_imageResource);
    set_title("Image button example");
    set_border_width(40);

    m_toggleButton.set_image(m_image);

    m_toggleButton.signal_clicked().connect(sigc::mem_fun(*this, 
        &ExampleWindow::on_clicked_button));

    add(m_toggleButton);
    show_all_children();
}

ExampleWindow::~ExampleWindow()
{

}

void ExampleWindow::on_clicked_button()
{
    if(m_toggleButton.get_active()){
        cout << "Button activated !" << endl;
    }
    else 
        cout << "Button deactivated !" << endl;
}